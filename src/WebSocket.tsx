import { Observable } from 'rxjs';
import socketIo from 'socket.io-client';
 
let ENV = {
    TYS_SOCKET_API_URL: "http://localhost:4000"
};
export default class Socket {

  InitializeClientSocket() {
    const entityID = "nitin.gurbani@itpeoplecorp.com";
    return new Observable((observer) => {
      const socket = socketIo.connect(ENV.TYS_SOCKET_API_URL);
      socket.on(entityID, (data: {} | undefined) => {
        observer.next(data);
      });
      return observer;
    });
  }

  InitializePartnerSocket() {
     return new Observable((observer) => {
      const socket = socketIo.connect(ENV.TYS_SOCKET_API_URL);
      socket.on('partner', (data: {} | undefined) => {
        observer.next(data);
      });
      return observer;
    });
  }

  Emit(data: any) {
    console.log('emit', data);

     const socket = socketIo.connect(ENV.TYS_SOCKET_API_URL);
    console.log('After connect');
    socket.emit('TYS_EVENT', data, (d: any) => {
      console.log('sent');
    });
  }
}