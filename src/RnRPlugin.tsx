import React, { useEffect, useState } from 'react';
import FIXTURES from './RnRData.json';

export interface Rating {
  id: string,
  href: string,
  eqyYear: number,
  period: string,
  financialDate: string,
  reportingPeriod: string,
  fhr: number,
  chs: number,
  probabilityDefault: number,
  termPd: {
    year2: number,
    year3: number,
    year4: number,
    year5: number,
    year6: number,
    year7: number,
    year8: number,
    year9: number
    year10: number
  },
  delta: number,
  simulatedFhr: number,
  simulatedFhrDelta: number,
  operatingProfitability: number,
  netProfitability: number,
  capitalStructureEfficiency: number,
  costStructureEfficieny: number,
  leverage: string,
  liquidity: string,
  earningsPerformance: string,
}

export interface RnrMessage {
  id: string,
  // [key: string]: any
  name: string,
  country: string,
  ticker: string,
  exchange: string,
  sector: string,
  href: string,
  financials: string,
  periods: string,
  ratings: string,
  latestRatings: Rating[],
  reportLinks: {
    financialDialog: string,
    fhr: string,
  },
  address: string,
  reports: any[],
  sectorAverages: any,
  industryClassificationSystem: any,
  industryClassificationCode: any,
  dataScale: string,
  currency: string,
  dataConfidential: string,
  externalSourceId: any
}

export interface TysPluginProps {
  [key: string]: any
}

export function RnrPlugin(props: TysPluginProps) {
  const [data, setData] = useState<RnrMessage>(props.list);

  useEffect(() => {
    setData(props.list)
  },[props]);

  const {
    name,
    country,
    ticker,
    exchange,
    sector,
    latestRatings,
    reportLinks,
    address,
    sectorAverages,
  } = data

  return (
    <div className="container">
      <div className="row">
        <div style={{ textAlign: "left" }}>
          <h4>RapidRatings Financial Health Analysis</h4>
          <h3>{name} - {ticker} - {sector}</h3>
          <p>{address}</p>
        </div>
        <div></div>
      </div>
      <div className="row">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Eqy Year / Period</th>
              <th scope="col">Financial Date/Reporting Period</th>
              <th scope="col">FHR/CHS</th>
              <th scope="col">Probability Default</th>
              <th>Simulated FHR/Delta</th>
              <th>Operating/Net Profitability</th>
              <th>Leverage/Liquidity/Performance</th>
            </tr>
          </thead>
          <tbody>
            {latestRatings && latestRatings.map((rating: Rating) => {
              const {
                eqyYear,
                period,
                financialDate,
                reportingPeriod,
                probabilityDefault,
                fhr,
                chs,
                simulatedFhr,
                simulatedFhrDelta,
                operatingProfitability,
                netProfitability,
                leverage,
                liquidity,
                earningsPerformance
              } = rating

              return (
                <tr>
                  <td>{eqyYear} / {period}</td>
                  <td>{financialDate} / {reportingPeriod}</td>
                  <td>{fhr} / {chs}</td>
                  <td>{probabilityDefault}</td>
                  <td>{simulatedFhr} / {simulatedFhrDelta}</td>
                  <td>{operatingProfitability} / {netProfitability}</td>
                  <td>{leverage} / {liquidity} / {earningsPerformance}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      <hr/>
      <div className="row">
        <a type="button" className="btn btn-primary" href={reportLinks && reportLinks.financialDialog} target="_blank">Download Financial Dialog</a>
        <a type="button" className="btn btn-primary" href={reportLinks && reportLinks.fhr} target="_blank">Download FHR Report</a>
      </div>
    </div>
  )
}
