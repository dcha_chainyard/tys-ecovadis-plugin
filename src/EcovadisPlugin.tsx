import React from 'react'

import FIXTURES from './fixtures.json'

export interface EcovadisCompanyDetails {
  sustainableProcurementData: {
    ecovadisRating: {
      hasRating: string,
      hasMedal: string,
      medalType: "gold" | "silver" | "bronze",
      medalIcon: {
        rasterUri: string,
        vectorUri: string
      },
      issueDate: string,
      expiryDate: string
    }
  },
  companyData: {
    EVId: {
      value: string
    },
    companyName: string,
    countryCode: string,
    countryName: string,
    cityName: string,
    zipCode: string,
    companyWebsite: string
  }
}

export interface EcovadisCompanySearchResult {
  search: {
    code: number,
    message: string,
    count: number,
    "next-page": boolean
  },
  companyIdentification: {
    EVId: {
      value: string
    },
    _link: string
  }[]
}

export interface EcovadisApi {
  searchCompany: (duns: string) => Promise<EcovadisCompanySearchResult>
  getCompanyDetails: (evid: string) => Promise<EcovadisCompanyDetails>
}

export function useEcovadisApi(): EcovadisApi {
  async function login() {
  }

  async function searchCompany(duns: string) {
    return {
      "search": {
          "code": 1,
          "message": "OK",
          "count": 1,
          "next-page": false
      },
      "companyIdentification": [
          {
              "EVId": {
                  "value": "MI124918"
              },
              "_link": "https://data-sandbox.ecovadis-survey.com/companies/v0.1/company/MI124918"
          }
      ]
    }
  }

  async function getCompanyDetails(evid: string): Promise<EcovadisCompanyDetails> {
    const details = (FIXTURES as { [evid: string]: EcovadisCompanyDetails })[evid] 

    return details
  }

  return {
    searchCompany,
    getCompanyDetails
  }
}

export function EcovadisPlugin(props: { duns: string }) {
  const [companyDetails, setCompanyDetails] = React.useState<EcovadisCompanyDetails>()

  const ecovadisApi = useEcovadisApi()

  React.useEffect(() => {
    ecovadisApi.getCompanyDetails("MI124918")
      .then(details => setCompanyDetails(details))
  }, [ecovadisApi])

  return (
    <div style={{ display: "flex", justifyContent: "space-around", lineHeight: ".8rem"}}>
      <div style={{ textAlign: "left"}}>
        <h2>EcoVadis Data</h2>
        {companyDetails
          ? (<div>
            <h2><strong>{companyDetails.companyData.companyName}</strong></h2>
            <p style={{ color: "#aaa"}}>{companyDetails.companyData.cityName} - {companyDetails.companyData.countryCode} | <a style={{ color: "#aaa" }} href={`https://${companyDetails.companyData.companyWebsite}`} target="_noreferrer _noopener">Manufacture of electronic components and boards</a></p>
            <p style={{ color: "#aaa"}}>EVID {companyDetails.companyData.EVId.value}</p>
            <p><span style={{ backgroundColor: "red", color: "white", borderRadius: "50%", padding: '0 7px'}}>!</span> <span style={{ color: "red" }}>Risk country operations</span></p>
          </div>)
          : (<div>Loading...</div>)}
      </div>
      <div style={{ margin: "auto 0 auto 0"}}>
        {companyDetails 
          && companyDetails.sustainableProcurementData.ecovadisRating.hasRating 
          && companyDetails.sustainableProcurementData.ecovadisRating.hasMedal 
          && (
          <svg width="100" height="100">
            <image href={companyDetails.sustainableProcurementData.ecovadisRating.medalIcon.vectorUri} width="100" height="100"/>
          </svg>
        )}
      </div>
    </div>
  )
}