import React, {useState} from 'react';
import './App.css';
import { EcovadisPlugin } from './EcovadisPlugin';
import { RnrPlugin } from './RnRPlugin';
import Socket from './WebSocket';

type PluginContainerProps = {
  Component: (props: any) => JSX.Element,
  componentProps: any,
}

function PluginContainer(props: PluginContainerProps) {
  return (
    <div style={{ 
      border: "1px solid #eee", 
      margin: "1rem 10rem 1rem 10rem", 
      borderRadius: "3px", 
      boxShadow: "1px 1px 2px 2px #ddd",
      padding: '1rem'
    }}>
      <props.Component {...props.componentProps} />
    </div>
  )
}

const App: React.FC = () => {
  const [componentProps, setComponentProps] = useState({
    duns: "294036723",
    list: {}
  });

  const socket = React.useRef(new Socket());

  React.useEffect(() => {
    socket.current.Emit({ company: 'TYS',rrid:"7J1G2yA" });
    const subscription = socket.current.InitializePartnerSocket().subscribe((res: any) => {
      if (res) {
        const response = JSON.parse(res);
        setComponentProps({
          duns: "294036723",
          list: response.Payload.response[0],
        });
      }
    });

    return () => subscription.unsubscribe()
  }, [])

  return (
    <div className="App">
      <header><h1>TYS Ecovadis Plugin Demo</h1></header>
      <PluginContainer 
        Component={EcovadisPlugin}
        componentProps={componentProps}
      />
      <PluginContainer 
        Component={RnrPlugin}
        componentProps={componentProps}
      />
    </div>
  );
}

export default App;
